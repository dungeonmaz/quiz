const data = [
    {
        question: 'What is the biggest country?',
        a: 'Russia',
        b: 'USA',
        c: 'China',
        correct: 'a'
    },
    {
        question: "What's the biggest animal in the world?",
        a: 'Elephant',
        b: 'Whale',
        c: 'Giraffe',
        correct: 'b'
    },
    {
        question: 'How many valves does the heart have?',
        a: '5',
        b: '3',
        c: '4',
        correct: 'c'
    },
]

const question = document.getElementById('question')
const a_text = document.getElementById('a_text')
const b_text = document.getElementById('b_text')
const c_text = document.getElementById('c_text')
const submit = document.getElementById('submit')

let currentQuestion = 0
let score = 0
let answer_el = undefined

loadQuestion();



function getAnswer() {
    const answers = document.querySelectorAll('[name="answer"]')

    answers.forEach(answer => {
        if (answer.checked) {
            answer_el = answer.id;
        }
    })
}

function uncheckAnswers() {
    const answers = document.querySelectorAll('[name="answer"]')

    answers.forEach(answer => {
        answer.checked = false
        answer_el = undefined
    })
}


function loadQuestion() {
    const quiz = data[currentQuestion]

    question.innerText = quiz.question
    a_text.innerText = quiz.a
    b_text.innerText = quiz.b
    c_text.innerText = quiz.c
}

submit.addEventListener('click', () => {
    getAnswer()
    if (!answer_el) return

    if (answer_el == data[currentQuestion].correct) score++
    currentQuestion++
    if (currentQuestion < data.length) {
        loadQuestion()
    } else {
        alert(score)
    }
    uncheckAnswers()
})